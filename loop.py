for i in range(101):
    if i < 10:
        print("The following number is less than 10")
    if 10 <= i <= 50:
        print("The following number is between 10 and 50")
    if i > 50:
        print("The following number is greater than 50")
    print(i)